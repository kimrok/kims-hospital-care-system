package edu.ntnu.idatt2001.kimrok;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {

    @Test
    @DisplayName("Remove existing patient")
    void removeExistingPatient() throws RemoveException {
        // Adds test data
        Department department = new Department("Test");
        Patient patient = new Patient("Odd", "Even", "");
        department.addPatient(patient);

        // Test will fail if the Patient did not get removed and pass otherwise. No assert needed
        department.remove(patient);
    }


    @Test
    @DisplayName("Remove not existing patient")
    void removeNotExistingPatient() {
        // Adds test data
        Department department = new Department("Test");
        Patient patient = new Patient("Odd", "Even", "");

        // Checks if removing a patient that doesnt exist throws a RemoveException
        assertThrows(RemoveException.class, () -> {
            department.remove(patient);
        });
    }

}