package edu.ntnu.idatt2001.kimrok;

/**
 * Only the GeneralPractitioner and Surgeon classes can diagnose a patient.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0 2021.03.08
 * @since 2021.03.08
 */
public class Surgeon extends Doctor{

    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);

    }

    /**
     * Sets the diagnosis of an individual patient.
     *
     * @param patient
     * @param diagnosis
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
