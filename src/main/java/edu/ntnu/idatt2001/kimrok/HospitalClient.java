package edu.ntnu.idatt2001.kimrok;

/**
 * The class HospitalClient.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.1 2021.03.11
 * @since 2021.03.10
 */
public class HospitalClient {
    public static void main(String[] args) throws RemoveException {
        // Adds data from HospitalTestData
        Hospital hospital = new Hospital("Hoospit");
        HospitalTestData.fillRegisterWithTestData(hospital);

        // Takes the first person from the first department
        Department department = hospital.getDepartments().get(0);
        Person person = department.getPatients().get(0);

        // Test for removing existing person
        System.out.println("Removing person first time");
        department.remove(person);

        // Test for removing the person that no longer exist
        System.out.println("\nRemoving person second time");
        try {
            department.remove(person);
        } catch (RemoveException e) {
            System.out.println(e.error);
        }

    }
}
