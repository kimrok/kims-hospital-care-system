package edu.ntnu.idatt2001.kimrok;

/**
 * The class Diagnosable is an interface class that only has the method setDiagnose to be implemented by Patient.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0 2021.03.09
 * @since 2021.03.09
 */
interface Diagnosable {
    void setDiagnosis(String diagnosis);
}
