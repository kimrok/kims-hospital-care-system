package edu.ntnu.idatt2001.kimrok;

/**
 * The class Person is defined as abstract as it does not make sense to create objects from it.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0 2021.03.08
 * @since 2021.03.08
 *
 */
public abstract class Person {
    String firstName;
    String lastName;
    String socialSecurityNumber;

    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Combines the first and last name to get the full name of the person
     *
     * @return Full name with lastname first separated by a comma.
     */
    public String getFullName() {
        return lastName + ", " + firstName;
    }

    /**
     * @return first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * set first name of the person
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * set last name of the person
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return social security number
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * set the social security number of the person
     *
     * @param socialSecurityNumber
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * @return toString
     */
    @Override
    public String toString() {
        return "person " + lastName + ", " + firstName + " with social security number " + socialSecurityNumber;
    }
}
