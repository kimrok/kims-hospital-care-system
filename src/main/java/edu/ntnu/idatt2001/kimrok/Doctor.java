package edu.ntnu.idatt2001.kimrok;

/**
 * The class Doctor is defined as abstract as it does not make sense to create objects from it.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0 2021.03.08
 * @since 2021.03.08
 */
public abstract class Doctor extends Employee{

    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @param patient
     * @param diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
