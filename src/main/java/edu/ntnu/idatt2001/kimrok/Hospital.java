package edu.ntnu.idatt2001.kimrok;

import java.util.ArrayList;

/**
 * The class Hospital contains all departments
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0 2021.03.09
 * @since 2021.03.09
 */
public class Hospital {
    private String hospitalName;
    ArrayList<Department> departments;

    /**
     * @param hospitalName
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        this.departments = new ArrayList<>();
    }

    /**
     * @return Hospital name
     */
    public String getHospitalName () {
        return hospitalName;
    }

    /**
     * @return Array List of departments
     */
    public ArrayList<Department> getDepartments () {
        return this.departments;
    }

    /**
     * @param department
     */
    public void addDepartment(Department department) {
        this.departments.add(department);
    }

    /**
     * @return toString
     */
    @Override
    public String toString() {
        return hospitalName + " hospital";
    }
}
