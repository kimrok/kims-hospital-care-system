package edu.ntnu.idatt2001.kimrok;

/**
 * The class Nurse should not be able to diagnose a patient.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0 2021.03.08
 * @since 2021.03.08
 */
public class Nurse extends Employee{

    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     *
     * @return toString
     */
    @Override
    public String toString() {
        return "nurse " + lastName + ", " + firstName + " with social security number " + socialSecurityNumber;
    }
}
