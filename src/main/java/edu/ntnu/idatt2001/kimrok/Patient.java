package edu.ntnu.idatt2001.kimrok;

/**
 * @author Kim Johnstuen Rokling
 * @version 1.0 2021.03.08
 * @since 2021.03.08
 */
public class Patient extends Person implements Diagnosable{
    String diagnosis = "";

    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @param diagnosis
     */
    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * @return diagnosis
     */
    public String getDiagnosis() {
        return this.diagnosis;
    }

    /**
     *
     * @return toString
     */
    @Override
    public String toString() {
        return "patient " + lastName + ", " + firstName + " with social security number " + socialSecurityNumber;

    }
}
