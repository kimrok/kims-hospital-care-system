package edu.ntnu.idatt2001.kimrok;

/**
 * @author Kim Johnstuen Rokling
 * @version 1.0 2021.03.08
 * @since 2021.03.08
 */
public class Employee extends Person {

    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);

    }

    /**
     * @return toString
     */
    @Override
    public String toString() {
        return "employee " + lastName + ", " + firstName + " with social security number " + socialSecurityNumber;
    }
}
