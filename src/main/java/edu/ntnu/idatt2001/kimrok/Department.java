package edu.ntnu.idatt2001.kimrok;

import java.util.ArrayList;
import java.util.Objects;

/**
 * The class Department contains all employees and patients
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0 2021.03.09
 * @since 2021.03.09
 */
public class Department {
    private String departmentName;
    ArrayList<Employee> employees;
    ArrayList<Patient> patients;

    /**
     * @param departmentName
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.employees = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    /**
     * @return Department name
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * @param departmentName
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * @return Array List of employees
     */
    public ArrayList<Employee> getEmployees() {
        return this.employees;
    }

    /**
     * @param employee
     */
    public void addEmployee(Employee employee) {
        this.employees.add(employee);
    }

    /**
     * @return Array List of patients
     */
    public ArrayList<Patient> getPatients() {
        return this.patients;
    }

    /**
     * @param patient
     */
    public void addPatient(Patient patient) {
        this.patients.add(patient);
    }

    /**
     * Checks if the Person to be removed is a part of the employees or patients list and removes it.
     * Throws an exception RemoveException if the Person is in neither ArrayList.
     *
     * @param person
     */
    public void remove(Person person) throws RemoveException {

        if (employees.contains(person)) {
            employees.remove(person);
        } else if (patients.contains(person)) {
            patients.remove(person);
        } else {
            throw new RemoveException("Person not found: " + person.getFullName());
        }
    }

    /**
     * @param o
     * @return True if objects are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    /**
     * @return HashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    /**
     * @return toString
     */
    @Override
    public String toString() {
        return departmentName + " department";
    }
}
